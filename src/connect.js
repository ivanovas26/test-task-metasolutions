let fs = require("fs")

var entrystore = require("@entryscape/entrystore-js")

let es = new entrystore.EntryStore("https://recruit.entryscape.net/store")

const projection = {
    title: "http://purl.org/dc/terms/title",
    description: "http://purl.org/dc/terms/description",
    imgSrc: "http://xmlns.com/foaf/0.1/img",
    artist: "http://example.com/artist",
}

// collect pieces of art
es.newSolrQuery().rdfType("http://example.com/PieceOfArt").context("1").getEntries().then(function (entries) {
        let data = []

        // store pieces of art in data
        for(let entry of entries) {
            let proj = entry.projection(projection)
            proj.artistId = proj.artist.split("/").pop()
            data.push(proj)
        }

        // collect artists
        es.newSolrQuery().rdfType("http://example.com/Artist").context("1").getEntries().then(function (artists) {
            
            for(let artist of artists){
                //extract given and family names
                let proj = artist.projection({"givenName" : "http://xmlns.com/foaf/0.1/givenName",
                                                "familyName" : "http://xmlns.com/foaf/0.1/familyName"})
                // get number from artist URI
                let id = artist.getURI().split("/").pop()
                // if number matches the one in the URI we already have in data, insert names
                for(let d=0; d<data.length; d++){
                    if(data[d].artistId === id) data[d].artist = proj
                }
            }

            // write all to data.json
            fs.writeFileSync("./src/data.json", JSON.stringify(data))
        })
})