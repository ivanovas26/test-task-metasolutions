import React, { Component } from 'react'
import "./Gallery.css"

class ImgCard extends Component {
  constructor(props) {
    super(props)
    this.state = {
      imgPath: props.imgPath,
      text: props.text,
      description: props.description,
      artistGN: props.artistGN,
      artistFN: props.artistFN,
      id: props.id
    }
  }

  // opens the modal box
  componentDidMount(){
    let card = document.getElementById(this.state.id + "_card")
    let modal = document.getElementById(this.state.id + "_modal")
    card.onclick = () => { modal.style.display="inline-block" }
  }

  render() {
    return (
      <div id={this.state.id + "_card"} className="w3-card card">
        {/* image and title box */}
        <img className="w3-image w3-hover-sepia" src={this.state.imgPath} alt={this.state.text}/>
        <div className="w3-container box">
          <h5 className="titletext">{this.state.text}</h5>
        </div>
        
        {/* modal box */}
        <div id={this.state.id + "_modal"} className="w3-modal">

          <div className="w3-modal-content w3-animate-opacity w3-card-4">

            {/* header bar of the modal box */}
            <header className="w3-container w3-padding box">
              <span onClick={() => document.getElementById(this.state.id + "_modal").style.display="none"} 
                                                            className="w3-button w3-display-topright">&times;</span>
              <h2 className="modaltext">{this.state.text}</h2>
            </header>

            {/* description container */}
            <div className="w3-container box">
              <p className="modaltext">{this.state.description}</p>
            </div>

            {/* footer bar of the modal box */}
            <footer className="w3-container footer">
              <p className="artist-text">Artist: {this.state.artistGN} {this.state.artistFN}</p>
            </footer>
            
          </div>
        </div>

      </div>
    )
  }
}

export default ImgCard