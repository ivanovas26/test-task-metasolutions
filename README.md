This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

### Starting the app locally

`npm start`

### connect.js

This is a script which runs with Node due to the fact that it could not be run in the browser (mentioned in repo notes on entrystore).
The script grabs the data from https://recruit.entryscape.net/store, gets the entries for pieces of art and artists, creates projections of them, and writes all the data into data.json (see in src directory). 

connect.js is executed before starting the webpage.

To execute separately, use

`node src/connect.js`