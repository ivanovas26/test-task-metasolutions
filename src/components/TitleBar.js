import React, { Component } from 'react'

class TitleBar extends Component{
    constructor(props) {
        super(props);
        this.state = { }
    }

    stickify() {
        // get header
        let header = document.getElementById("navbar")

        // get offset position of the navbar
        let sticky = header.offsetTop;

        if (window.pageYOffset > sticky)
            header.classList.add("sticky")
        else
            header.classList.remove("sticky")
    }

    render() {
        // stick when scrolling
        window.onscroll = this.stickify

        return (
            <div className="w3-bar" id="navbar" style={{zIndex:"5000", maxHeight:"60px", backgroundColor: "rgb(247, 235, 213)"}}>
                <div className="w3-bar-item w3-xxlarge w3-tangerine">
                    <b>Artwork Gallery</b>
                </div>
            </div>
        )
    }
}

export default TitleBar