import React from 'react'
import './App.css'
import Gallery from './components/Gallery'
import TitleBar from './components/TitleBar'

function App() {
  
  return (
    <div className="App">
      <TitleBar />
      <Gallery />
    </div>
  )

}

export default App
