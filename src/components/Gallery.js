import React, { Component } from 'react'
import './Gallery.css'
import ImgCard from './ImgCard'
import data from '../data.json'

class Gallery extends Component{
    constructor(props) {
        super(props)
        this.state = { }
    }

    renderImgCard(imgPath, text, description, artist, id) {
        return (
            <ImgCard key={id + "_imgCard"} imgPath={imgPath} text={text} description={description} artistGN={artist.givenName} 
                                            artistFN={artist.familyName} id={id}/>
        )
    }

    // create full set of needed image cards
    createSet = () => {
        let set = []

        for(let i=0; i<data.length; i++)
        {
            set.push(this.renderImgCard(data[i].imgSrc, data[i].title, data[i].description, data[i].artist, i))
        }

        return set
    }

    render() {
        return (
            <div className="photos">
                {this.createSet()}
            </div>
        )
    }

}

export default Gallery